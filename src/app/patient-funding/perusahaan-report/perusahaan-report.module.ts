import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PerusahaanReportComponent } from './perusahaan-report.component';
import { PerusahaanReportRoutes } from './perusahaan-report.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PerusahaanReportRoutes),
      SharedModule
  ],
  declarations: [PerusahaanReportComponent]
})

export class PerusahaanReportModule {}
