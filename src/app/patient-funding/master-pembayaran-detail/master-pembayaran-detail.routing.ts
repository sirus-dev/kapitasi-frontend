import { Routes } from '@angular/router';

import { MasterPembayaranDetailComponent } from './master-pembayaran-detail.component';

export const MasterPembayaranDetailRoutes: Routes = [{
  path: '',
  component: MasterPembayaranDetailComponent,
  data: {
    breadcrumb: 'Detail Master Pembayaran',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
