import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TransaksiListComponent } from './transaksi-list.component';
import { TransaksiListRoutes } from './transaksi-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(TransaksiListRoutes),
      SharedModule,
      NgxDatatableModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [TransaksiListComponent]
})

export class TransaksiListModule {}
