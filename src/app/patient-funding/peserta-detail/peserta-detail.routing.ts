import { Routes } from '@angular/router';

import { PesertaDetailComponent } from './peserta-detail.component';

export const PesertaDetailRoutes: Routes = [{
  path: '',
  component: PesertaDetailComponent,
  data: {
    breadcrumb: 'Detail Peserta',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
