import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMSKLINIK = [
  {
    label: '',
    main: [
      {
        main_state: 'klinik',
        state: 'dashboard',
        name: 'Dashboard',
        type: 'link',
        icon: 'icofont icofont-home'
      },
      {
        main_state : 'klinik',
        state: 'profile',
        name: 'Profil Klinik',
        type: 'link',
        icon: 'icofont icofont-certificate-alt-1'
      },
      {
        main_state: 'klinik',
        state: 'perusahaan',
        name: 'Pemberi Kerja',
        type: 'sub',
        icon: 'icofont icofont-building',
        children: [
          {
            state: 'perusahaan-list',
            name: 'Info Pemberi Kerja'
          },
        ]
      },
      {
        main_state: 'klinik',
        state: 'rumahsakit',
        name: 'Rumah Sakit',
        type: 'sub',
        icon: 'icofont icofont-hospital',
        children: [
          {
            state: 'rumahsakit-list',
            name: 'Info Rumah Sakit'
          }
        ]
      },
      {
        main_state : 'klinik',
        state: 'peserta',
        name: 'Anggota',
        type: 'sub',
        icon: 'icofont icofont-user-alt-3',
        children: [
          {
            state: 'peserta-list',
            name: 'Info Anggota'
          },
        ]
      },
    ]
  },
];

@Injectable()
export class MenuItemsKlinik {
  getAll(): Menu[] {
    return MENUITEMSKLINIK;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
