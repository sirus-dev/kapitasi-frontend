import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { MasterPembayaranAddComponent } from './master-pembayaran-add.component';
import { MasterPembayaranAddRoutes } from './master-pembayaran-add.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(MasterPembayaranAddRoutes),
      SharedModule
  ],
  declarations: [MasterPembayaranAddComponent]
})

export class MasterPembayaranAddModule {}
