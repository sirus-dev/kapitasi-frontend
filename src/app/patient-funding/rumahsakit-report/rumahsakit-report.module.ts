import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RumahSakitReportComponent } from './rumahsakit-report.component';
import { RumahSakitReportRoutes } from './rumahsakit-report.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RumahSakitReportRoutes),
      SharedModule
  ],
  declarations: [RumahSakitReportComponent]
})

export class RumahSakitReportModule {}
