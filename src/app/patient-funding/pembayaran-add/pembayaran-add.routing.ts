import { Routes } from '@angular/router';

import { PembayaranAddComponent } from './pembayaran-add.component';

export const PembayaranAddRoutes: Routes = [{
  path: '',
  component: PembayaranAddComponent,
  data: {
    breadcrumb: 'Tambah Pembayaran',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
