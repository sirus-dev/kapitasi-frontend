import { Routes } from '@angular/router';

import { KlinikEditComponent } from './klinik-edit.component';

export const KlinikEditRoutes: Routes = [{
  path: '',
  component: KlinikEditComponent,
  data: {
    breadcrumb: 'Ubah Klinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
