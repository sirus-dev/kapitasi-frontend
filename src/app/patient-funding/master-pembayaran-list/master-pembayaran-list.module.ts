import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { MasterPembayaranListComponent } from './master-pembayaran-list.component';
import { MasterPembayaranDetailComponent } from '../master-pembayaran-detail/master-pembayaran-detail.component';
import { MasterPembayaranEditComponent } from '../master-pembayaran-edit/master-pembayaran-edit.component';
import { MasterPembayaranListRoutes } from './master-pembayaran-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(MasterPembayaranListRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [MasterPembayaranListComponent, MasterPembayaranDetailComponent, MasterPembayaranEditComponent]
})

export class MasterPembayaranListModule {}
