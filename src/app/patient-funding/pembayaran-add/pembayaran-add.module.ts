import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PembayaranAddComponent } from './pembayaran-add.component';
import { PembayaranAddRoutes } from './pembayaran-add.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PembayaranAddRoutes),
      SharedModule
  ],
  declarations: [PembayaranAddComponent]
})

export class PembayaranAddModule {}
