import { Routes } from '@angular/router';

import { MasterPembayaranEditComponent } from './master-pembayaran-edit.component';

export const MasterPembayaranEditRoutes: Routes = [{
  path: '',
  component: MasterPembayaranEditComponent,
  data: {
    breadcrumb: 'Ubah Master Pembayaran',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
