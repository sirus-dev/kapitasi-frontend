import { Routes } from '@angular/router';

import { PerusahaanDetailComponent } from './perusahaan-detail.component';

export const PerusahaanDetailRoutes: Routes = [{
  path: '',
  component: PerusahaanDetailComponent,
  data: {
    breadcrumb: 'PerusahaanDetail',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
