import {Component, OnInit} from '@angular/core';
declare const $: any;
declare var Morris: any;

import * as XLSX from 'xlsx';

type AOA = any[][];

@Component({
  selector: 'app-peserta-add',
  templateUrl: './peserta-add.component.html'
})

export class PesertaAddComponent implements OnInit {

  arrayBuffer: any;
  file: File;
  dataTable: Array<any>;

  constructor() { }

  ngOnInit() {
  }

  incomingfile(event) {
    this.file = event.target.files[0];
  }

  upload() {
    let sheet = [];
    const fileReader = new FileReader();
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            const data = new Uint8Array(this.arrayBuffer);
            const arr = new Array();
            for (let i = 0; i !== data.length; ++i) {
              arr[i] = String.fromCharCode(data[i]);
            }
            const bstr = arr.join('');
            const workbook = XLSX.read(bstr, {type: 'binary'});
            const first_sheet_name = workbook.SheetNames[0];
            const worksheet = workbook.Sheets[first_sheet_name];
            sheet = XLSX.utils.sheet_to_json(worksheet, { raw: true });
            const newdata = sheet.map((o: any, i) => {
              const index = {NO: i + 1};
              return Object.assign(index, o);
            });
            this.dataTable = newdata;
            console.log(JSON.stringify(this.dataTable));
        };
        fileReader.readAsArrayBuffer(this.file);
  }
}
