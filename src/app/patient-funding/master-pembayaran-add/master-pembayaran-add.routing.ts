import { Routes } from '@angular/router';

import { MasterPembayaranAddComponent } from './master-pembayaran-add.component';

export const MasterPembayaranAddRoutes: Routes = [{
  path: '',
  component: MasterPembayaranAddComponent,
  data: {
    breadcrumb: 'Tambah Master Pembayaran',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
