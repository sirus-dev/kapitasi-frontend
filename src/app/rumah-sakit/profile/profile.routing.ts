import { Routes } from '@angular/router';

import { ProfileComponent } from './profile.component';

export const ProfileRoutes: Routes = [{
  path: '',
  component: ProfileComponent,
  data: {
    breadcrumb: 'Profil Rumah Sakit',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
