import { Routes } from '@angular/router';

import { PaketDetailComponent } from './paket-detail.component';

export const PaketDetailRoutes: Routes = [{
  path: '',
  component: PaketDetailComponent,
  data: {
    breadcrumb: 'Detail Paket',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
