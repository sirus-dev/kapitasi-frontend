import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PaketListComponent } from './paket-list.component';
import { PaketDetailComponent } from '../paket-detail/paket-detail.component';
import { PaketEditComponent } from '../paket-edit/paket-edit.component';
import { PaketListRoutes } from './paket-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PaketListRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PaketListComponent, PaketDetailComponent, PaketEditComponent]
})

export class PaketListModule {}
