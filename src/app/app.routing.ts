import {Routes} from '@angular/router';

import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';
import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {PatientFundingLayoutComponent} from './layouts/patient-funding/patient-funding-layout.component';
import {PerusahaanLayoutComponent} from './layouts/perusahaan/perusahaan-layout.component';
import {RumahSakitLayoutComponent} from './layouts/rumah-sakit/rumah-sakit-layout.component';
import {KlinikLayoutComponent} from './layouts/klinik/klinik-layout.component';
import {BJBLayoutComponent} from './layouts/bjb/bjb-layout.component';

export const AppRoutes: Routes = [
{
  path: '',
  redirectTo: 'auth/login',
  pathMatch: 'full'
},
{
  path: 'patient-funding',
  component: PatientFundingLayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },
    {
      path: 'dashboard',
      loadChildren: './patient-funding/dashboard/dashboard.module#DashboardModule'
    },
    {
      path: 'profile',
      loadChildren: './patient-funding/profile/profile.module#ProfileModule'
    },
    {
      path: 'transaksi/transaksi-list',
      loadChildren: './patient-funding/transaksi-list/transaksi-list.module#TransaksiListModule'
    },
    {
      path: 'transaksi/transaksi-report',
      loadChildren: './patient-funding/transaksi-report/transaksi-report.module#TransaksiReportModule'
    },
    {
      path: 'pembayaran/pembayaran-list',
      loadChildren: './patient-funding/pembayaran-list/pembayaran-list.module#PembayaranListModule'
    },
    {
      path: 'pembayaran/pembayaran-add',
      loadChildren: './patient-funding/pembayaran-add/pembayaran-add.module#PembayaranAddModule'
    },
    {
      path: 'perusahaan/perusahaan-list',
      loadChildren: './patient-funding/perusahaan-list/perusahaan-list.module#PerusahaanListModule'
    },
    {
      path: 'perusahaan/perusahaan-add',
      loadChildren: './patient-funding/perusahaan-add/perusahaan-add.module#PerusahaanAddModule'
    },
    {
      path: 'perusahaan/perusahaan-report',
      loadChildren: './patient-funding/perusahaan-report/perusahaan-report.module#PerusahaanReportModule'
    },
    {
      path: 'rumahsakit/rumahsakit-list',
      loadChildren: './patient-funding/rumahsakit-list/rumahsakit-list.module#RumahSakitListModule'
    },
    {
      path: 'rumahsakit/rumahsakit-add',
      loadChildren: './patient-funding/rumahsakit-add/rumahsakit-add.module#RumahSakitAddModule'
    },
    {
      path: 'rumahsakit/rumahsakit-report',
      loadChildren: './patient-funding/rumahsakit-report/rumahsakit-report.module#RumahSakitReportModule'
    },
    {
      path: 'klinik/klinik-list',
      loadChildren: './patient-funding/klinik-list/klinik-list.module#KlinikListModule'
    },
    {
      path: 'klinik/klinik-add',
      loadChildren: './patient-funding/klinik-add/klinik-add.module#KlinikAddModule'
    },
    {
      path: 'klinik/klinik-report',
      loadChildren: './patient-funding/klinik-report/klinik-report.module#KlinikReportModule'
    },
    {
      path: 'peserta/peserta-list',
      loadChildren: './patient-funding/peserta-list/peserta-list.module#PesertaListModule'
    },
    {
      path: 'peserta/peserta-add',
      loadChildren: './patient-funding/peserta-add/peserta-add.module#PesertaAddModule'
    },
    {
      path: 'peserta/peserta-report',
      loadChildren: './patient-funding/peserta-report/peserta-report.module#PesertaReportModule'
    },
    {
      path: 'user-access/user-list',
      loadChildren: './patient-funding/user-list/user-list.module#UserListModule'
    },
    {
      path: 'user-access/user-add',
      loadChildren: './patient-funding/user-add/user-add.module#UserAddModule'
    },
    {
      path: 'user-access/user-edit',
      loadChildren: './patient-funding/user-edit/user-edit.module#UserEditModule'
    },
    {
      path: 'paket/paket-list',
      loadChildren: './patient-funding/paket-list/paket-list.module#PaketListModule'
    },
    {
      path: 'paket/paket-add',
      loadChildren: './patient-funding/paket-add/paket-add.module#PaketAddModule'
    },
    {
      path: 'paket/paket-edit',
      loadChildren: './patient-funding/paket-edit/paket-edit.module#PaketEditModule'
    },
    {
      path: 'master-pembayaran/master-pembayaran-list',
      loadChildren: './patient-funding/master-pembayaran-list/master-pembayaran-list.module#MasterPembayaranListModule'
    },
    {
      path: 'master-pembayaran/master-pembayaran-add',
      loadChildren: './patient-funding/master-pembayaran-add/master-pembayaran-add.module#MasterPembayaranAddModule'
    },
    {
      path: 'master-pembayaran/master-pembayaran-edit',
      loadChildren: './patient-funding/master-pembayaran-edit/master-pembayaran-edit.module#MasterPembayaranEditModule'
    }
  ]
},
{
  path: 'perusahaan',
  component: PerusahaanLayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },
    {
      path: 'dashboard',
      loadChildren: './perusahaan/dashboard/dashboard.module#DashboardModule'
    },
    {
      path: 'profile',
      loadChildren: './perusahaan/profile/profile.module#ProfileModule'
    },
    {
      path: 'peserta/peserta-list',
      loadChildren: './perusahaan/peserta-list/peserta-list.module#PesertaListModule'
    },
    {
      path: 'peserta/peserta-add',
      loadChildren: './perusahaan/peserta-add/peserta-add.module#PesertaAddModule'
    },
    {
      path: 'peserta/peserta-report',
      loadChildren: './perusahaan/peserta-report/peserta-report.module#PesertaReportModule'
    },
    {
      path: 'rumahsakit/rumahsakit-list',
      loadChildren: './perusahaan/rumahsakit-list/rumahsakit-list.module#RumahSakitListModule'
    },
    {
      path: 'klinik/klinik-list',
      loadChildren: './perusahaan/klinik-list/klinik-list.module#KlinikListModule'
    },
    {
      path: 'transaksi/transaksi-list',
      loadChildren: './perusahaan/transaksi-list/transaksi-list.module#TransaksiListModule'
    },
    {
      path: 'transaksi/transaksi-add',
      loadChildren: './perusahaan/transaksi-add/transaksi-add.module#TransaksiAddModule'
    },
    {
      path: 'transaksi/transaksi-report',
      loadChildren: './perusahaan/transaksi-report/transaksi-report.module#TransaksiReportModule'
    },
  ]
},
{
  path: 'rumah-sakit',
  component: RumahSakitLayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },
    {
      path: 'dashboard',
      loadChildren: './rumah-sakit/dashboard/dashboard.module#DashboardModule'
    },
    {
      path: 'profile',
      loadChildren: './rumah-sakit/profile/profile.module#ProfileModule'
    },
    {
      path: 'perusahaan/perusahaan-list',
      loadChildren: './rumah-sakit/perusahaan-list/perusahaan-list.module#PerusahaanListModule'
    },
    {
      path: 'klinik/klinik-list',
      loadChildren: './rumah-sakit/klinik-list/klinik-list.module#KlinikListModule'
    },
    {
      path: 'peserta/peserta-list',
      loadChildren: './rumah-sakit/peserta-list/peserta-list.module#PesertaListModule'
    },
  ]
},
{
  path: 'klinik',
  component: KlinikLayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },
    {
      path: 'dashboard',
      loadChildren: './klinik/dashboard/dashboard.module#DashboardModule'
    },
    {
      path: 'profile',
      loadChildren: './klinik/profile/profile.module#ProfileModule'
    },
    {
      path: 'perusahaan/perusahaan-list',
      loadChildren: './klinik/perusahaan-list/perusahaan-list.module#PerusahaanListModule'
    },
    {
      path: 'rumahsakit/rumahsakit-list',
      loadChildren: './klinik/rumahsakit-list/rumahsakit-list.module#RumahSakitListModule'
    },
    {
      path: 'peserta/peserta-list',
      loadChildren: './klinik/peserta-list/peserta-list.module#PesertaListModule'
    },
  ]
},
{
  path: 'bjb',
  component: BJBLayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },
    {
      path: 'dashboard',
      loadChildren: './bjb/dashboard/dashboard.module#DashboardModule'
    },
    {
      path: 'profile',
      loadChildren: './bjb/profile/profile.module#ProfileModule'
    },
    {
      path: 'transaksi/transaksi-list',
      loadChildren: './bjb/transaksi-list/transaksi-list.module#TransaksiListModule'
    },
    {
      path: 'transaksi/transaksi-confirm',
      loadChildren: './bjb/transaksi-confirm/transaksi-confirm.module#TransaksiConfirmModule'
    },
    {
      path: 'transaksi/transaksi-report',
      loadChildren: './bjb/transaksi-report/transaksi-report.module#TransaksiReportModule'
    },
    {
      path: 'pembayaran/pembayaran-list',
      loadChildren: './bjb/pembayaran-list/pembayaran-list.module#PembayaranListModule'
    },
    {
      path: 'perusahaan/perusahaan-list',
      loadChildren: './bjb/perusahaan-list/perusahaan-list.module#PerusahaanListModule'
    },
    {
      path: 'perusahaan/perusahaan-report',
      loadChildren: './bjb/perusahaan-report/perusahaan-report.module#PerusahaanReportModule'
    },
    {
      path: 'rumahsakit/rumahsakit-list',
      loadChildren: './bjb/rumahsakit-list/rumahsakit-list.module#RumahSakitListModule'
    },
    {
      path: 'rumahsakit/rumahsakit-report',
      loadChildren: './bjb/rumahsakit-report/rumahsakit-report.module#RumahSakitReportModule'
    },
    {
      path: 'klinik/klinik-list',
      loadChildren: './bjb/klinik-list/klinik-list.module#KlinikListModule'
    },
    {
      path: 'klinik/klinik-report',
      loadChildren: './bjb/klinik-report/klinik-report.module#KlinikReportModule'
    },
    {
      path: 'peserta/peserta-list',
      loadChildren: './bjb/peserta-list/peserta-list.module#PesertaListModule'
    },
    {
      path: 'peserta/peserta-report',
      loadChildren: './bjb/peserta-report/peserta-report.module#PesertaReportModule'
    },
  ]
},
{
  path: '',
  component: AuthLayoutComponent,
  children: [
    {
      path: 'auth',
      loadChildren: './auth/auth.module#AuthModule'
    },
    {
      path: 'error',
      loadChildren: './error/error.module#ErrorModule'
    }
  ]
},
{
  path: '**',
  redirectTo: 'error/404'
},

];
