import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { MasterPembayaranEditComponent } from './master-pembayaran-edit.component';
import { MasterPembayaranEditRoutes} from './master-pembayaran-edit.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(MasterPembayaranEditRoutes),
      SharedModule
  ],
  declarations: [MasterPembayaranEditComponent]
})

export class MasterPembayaranEditModule {}
