import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RumahSakitDetailComponent } from './rumahsakit-detail.component';
import { RumahSakitDetailRoutes } from './rumahsakit-detail.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RumahSakitDetailRoutes),
      SharedModule
  ],
  declarations: [RumahSakitDetailComponent]
})

export class RumahSakitEditModule {}
