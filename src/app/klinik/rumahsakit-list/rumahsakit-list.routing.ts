import { Routes } from '@angular/router';

import { RumahSakitListComponent } from './rumahsakit-list.component';

export const RumahSakitListRoutes: Routes = [{
  path: '',
  component: RumahSakitListComponent,
  data: {
    breadcrumb: 'Daftar Rumah Sakit',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
