import { Routes } from '@angular/router';

import { UserEditComponent } from './user-edit.component';

export const UserEditRoutes: Routes = [{
  path: '',
  component: UserEditComponent,
  data: {
    breadcrumb: 'Ubah User',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
