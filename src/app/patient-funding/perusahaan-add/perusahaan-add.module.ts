import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PerusahaanAddComponent } from './perusahaan-add.component';
import { PerusahaanAddRoutes } from './perusahaan-add.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PerusahaanAddRoutes),
      SharedModule
  ],
  declarations: [PerusahaanAddComponent]
})

export class PerusahaanAddModule {}
