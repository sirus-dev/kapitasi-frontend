import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PaketAddComponent } from './paket-add.component';
import { PaketAddRoutes } from './paket-add.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PaketAddRoutes),
      SharedModule
  ],
  declarations: [PaketAddComponent]
})

export class PaketAddModule {}
