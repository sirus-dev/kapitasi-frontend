import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PesertaReportComponent } from './peserta-report.component';
import { PesertaReportRoutes } from './peserta-report.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PesertaReportRoutes),
      SharedModule,
      NgxDatatableModule
  ],
  declarations: [PesertaReportComponent]
})

export class PesertaReportModule {}
