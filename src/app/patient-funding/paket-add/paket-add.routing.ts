import { Routes } from '@angular/router';

import { PaketAddComponent } from './paket-add.component';

export const PaketAddRoutes: Routes = [{
  path: '',
  component: PaketAddComponent,
  data: {
    breadcrumb: 'Tambah Paket',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
