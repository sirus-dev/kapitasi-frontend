import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { UserAddComponent } from './user-add.component';
import { UserAddRoutes } from './user-add.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(UserAddRoutes),
      SharedModule
  ],
  declarations: [UserAddComponent]
})

export class UserAddModule {}
