import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TransaksiAddComponent } from './transaksi-add.component';
import { TransaksiAddRoutes } from './transaksi-add.routing';
import {SharedModule} from '../../shared/shared.module';
import { MonthDatePickerComponent } from '../../shared/month-date-picker/month-date-picker.component';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(TransaksiAddRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [TransaksiAddComponent, MonthDatePickerComponent]
})

export class TransaksiAddModule {}
