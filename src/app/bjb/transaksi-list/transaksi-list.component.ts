import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-transaksi-list',
  templateUrl: './transaksi-list.component.html',
  animations: [fadeInOutTranslate]
})

export class TransaksiListComponent implements OnInit {
  public loading = false;
  private transaksi_url = '/api/transaksis';
  private transaksidetail_url = '/api/transaksidetails';

  public id_perusahaan: string = '5c1092e408950336e0982829';
  public kode_perusahaan: string = '';
  public nama_perusahaan: string = '';

  position: string = 'bottom-right';
  title: string;
  msg: string;
  showClose: boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: string = 'default';
  closeOther: boolean = false;

  // @ViewChild(DatatableComponent) table: DatatableComponent;
  // loadingIndicator: Boolean = true;
  // reorderable: Boolean = true;
  // showDialog: Boolean = false;

  // columns = [
  //   { name: '_id', sortable: true},
  //   { name: '_id', sortable: true},
  //   { name: 'periode', sortable: true },
  //   { name: 'tanggal_bayar', sortable: true },
  //   { name: 'jumlah_bayar' , sortable: true },
  //   { name: 'konfirm_status' , sortable: true },
  //   { name: 'konfirm_tanggal', sortable: true },
  //   { name: '_id', sortable: true }
  // ];

  rowsFilter = [];
  tempFilter = [];
  rowsDetail = [];
  rowsSelected = {};

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router) {
    this.getData((data) => {
      this.tempFilter = [...data];
      this.rowsFilter = data;
    });
  }

  ngOnInit() {
    this.kode_perusahaan = 'P0001';
    this.nama_perusahaan = 'Perusahaan 1';
  }

  getData(cb) {

    const transaksi_json = {};
    this.loading = true;
    this.httpRequest.httpGet(this.transaksi_url + '?_sort=_id:DESC', transaksi_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }


  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.periode.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    // this.table.offset = 0;
  }


  changeStatus(id, status) {
    const transaksi_val = {};
    const transaksidetail_val = {};
    transaksi_val['_id'] = id;
    transaksi_val['konfirm_status'] = status;
    transaksi_val['konfirm_tanggal'] = new Date();
    const transaksi_json = JSON.stringify(transaksi_val);
    const transaksidetail_json = JSON.stringify(transaksidetail_val);
    // console.log(transaksi_json);

    // change status transaksi
    this.loading = true;
    this.httpRequest.httpPut(this.transaksi_url + '/' + id, transaksi_json).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);

          // get data detail transaksi
          this.httpRequest.httpGet(this.transaksidetail_url + '?transaksi=' + id, transaksidetail_json).subscribe(
            result2 => {
              try {
                // console.log(result);
                const result_msg2 = JSON.parse(result2._body);
                this.rowsDetail = result_msg2;

                this.rowsDetail.forEach(element => {

                  const transaksidetail_val2 = {};
                  transaksidetail_val2['konfirm_status'] = status;
                  transaksidetail_val2['konfirm_tanggal'] = new Date();
                  const transaksidetail_json2 = JSON.stringify(transaksidetail_val2);
                  // console.log(transaksidetail_json);

                  // change each detail transaksi
                  this.httpRequest.httpPut(this.transaksidetail_url + '/' + element._id, transaksidetail_json2).subscribe(
                    result3 => {
                      try {
                        const result_msg3 = JSON.parse(result3._body);
                        // console.log(result_msg2);
                        // console.log(result_msg2._id);

                      } catch (error) {
                        this.loading = false;
                        this.errorMessage.openErrorSwal('Something wrong.');
                      }
                    },
                    error => {
                      console.log(error);
                      this.loading = false;
                    }
                  );

                });


              } catch (error) {
                this.errorMessage.openErrorSwal('Something wrong.');
              }
            },
            error => {
              console.log(error);
              this.loading = false;
            }
          );

          this.loading = false;
          this.getData((data) => {
            this.tempFilter = [...data];
            this.rowsFilter = data;
          });

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  showDetail(dataRows) {
    this.rowsSelected = dataRows;
    this.loading = true;
    const transaksidetail_json = {};
    this.httpRequest.httpGet(this.transaksidetail_url + '?transaksi=' + dataRows._id, transaksidetail_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.rowsDetail = result_msg;
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
    document.getElementById('btnShowDetail').click();

  }

  showConfirm(event, id, status) {

    if (status === true) {
      swal({
        title: 'Batal Konfirmasi',
        text: 'Anda yakin membatalkan konfirmasi transaksi ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        confirmButtonClass: 'btn btn-success m-r-10',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this.changeStatus(id, !status);
          swal(
              'Info!',
              'Transaksi berhasil dibatalkan.',
              'success'
          );
        }
      });
    }else {
      swal({
        title: 'Konfirmasi',
        text: 'Anda yakin mengkonfirmasi transaksi ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        confirmButtonClass: 'btn btn-success m-r-10',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this.changeStatus(id, !status);
          swal(
              'Info!',
              'Transaksi berhasil dikonfirmasi.',
              'success'
          );
        }
      });
    }

  }

  addToast(options) {
    if (options.closeOther) {
      this.toastyService.clearAll();
    }
    this.position = options.position ? options.position : this.position;
    const toastOptions: ToastOptions = {
      title: options.title,
      msg: options.msg,
      showClose: options.showClose,
      timeout: options.timeout,
      theme: options.theme,
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added removed!');
      }
    };

    switch (options.type) {
      case 'default': this.toastyService.default(toastOptions); break;
      case 'info': this.toastyService.info(toastOptions); break;
      case 'success': this.toastyService.success(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
    }
  }
}
