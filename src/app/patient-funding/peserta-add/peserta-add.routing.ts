import { Routes } from '@angular/router';

import { PesertaAddComponent } from './peserta-add.component';

export const PesertaAddRoutes: Routes = [{
  path: '',
  component: PesertaAddComponent,
  data: {
    breadcrumb: 'Tambah Peserta',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
