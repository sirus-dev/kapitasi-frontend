import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PerusahaanEditComponent } from './perusahaan-edit.component';
import { PerusahaanEditRoutes } from './perusahaan-edit.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PerusahaanEditRoutes),
      SharedModule
  ],
  declarations: [PerusahaanEditComponent]
})

export class PerusahaanEditModule {}
