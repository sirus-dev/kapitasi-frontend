import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-paket-list',
  templateUrl: './paket-list.component.html',
  animations: [fadeInOutTranslate]
})

export class PaketListComponent implements OnInit {
  public loading = false;
  private paket_url = '/api/pakets';

  @ViewChild(DatatableComponent) table: DatatableComponent;
  loadingIndicator: Boolean = true;
  reorderable: Boolean = true;
  showDialog: Boolean = false;

  columns = [
    { prop: '_id' },
    { name: 'paketname' },
    { name: 'email' },
    { name: 'role' },
    { name: 'last_login' },
    { name: 'action' }
  ];

  rowsFilter = [];
  tempFilter = [];

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router) {
    this.getData((data) => {
      this.tempFilter = [...data];
      this.rowsFilter = data;
    });
  }

  ngOnInit() {
  }

  getData(cb) {

    const paket_json = {};
    this.loading = true;
    this.httpRequest.httpGet(this.paket_url + '?status_aktif=true', paket_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  deleteData(id) {
    const paket_val = {};
    paket_val['_id'] = id;
    paket_val['confirmed'] = false;
    const paket_json = JSON.stringify(paket_val);
    // console.log(paket_json);

    this.loading = true;
    this.httpRequest.httpPut(this.paket_url + '/' + id, paket_json).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);

          this.loading = false;
          this.getData((data) => {
            this.tempFilter = [...data];
            this.rowsFilter = data;
          });

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.email.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }

  updateStatus(event) {
    const val = event.target.value.toLowerCase();

    console.log(val);
    const temp = this.tempFilter.filter(function(d) {
      return d.role.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }


  showConfirmDelete(event, id) {
    swal({
      title: 'Konfirmasi Hapus',
      text: 'Anda yakin menghapus paket ini?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success m-r-10',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.deleteData(id);
        swal(
            'Info!',
            'Paket berhasil dihapus.',
            'success'
        );
      }
    });
  }


  showDetail() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowDetail').click();
    }.bind(this), 2000);
  }

  showEdit() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowEdit').click();
    }.bind(this), 2000);
  }


}
