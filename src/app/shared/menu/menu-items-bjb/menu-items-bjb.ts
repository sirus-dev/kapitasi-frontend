import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMSBJB = [
  {
    label: '',
    main: [
      {
        main_state : 'bjb',
        state: 'dashboard',
        name: 'Dashboard',
        type: 'link',
        icon: 'icofont icofont-home'
      },
      {
        main_state : 'bjb',
        state: 'profile',
        name: 'Profil BJB',
        type: 'link',
        icon: 'icofont icofont-certificate-alt-1'
      },
      {
        main_state : 'bjb',
        state: 'transaksi',
        name: 'Iuran',
        type: 'sub',
        icon: 'icofont icofont-retweet',
        children: [
          {
            state: 'transaksi-list',
            name: 'Data Iuran'
          },
          {
            state: 'transaksi-confirm',
            name: 'Konfirmasi Iuran'
          },
          {
            state: 'transaksi-report',
            name: 'Laporan Iuran'
          }
        ]
      },
      {
        main_state : 'bjb',
        state: 'pembayaran',
        name: 'Pembayaran',
        type: 'sub',
        icon: 'icofont icofont-cur-dollar',
        children: [
          {
            state: 'pembayaran-list',
            name: 'Data Pembayaran'
          },
        ]
      },
      {
        main_state : 'bjb',
        state: 'perusahaan',
        name: 'Pemberi Kerja',
        type: 'sub',
        icon: 'icofont icofont-building',
        children: [
          {
            state: 'perusahaan-list',
            name: 'Data Pemberi Kerja'
          },
          {
            state: 'perusahaan-report',
            name: 'Laporan Pemberi Kerja'
          },
        ]
      },
      {
        main_state : 'bjb',
        state: 'rumahsakit',
        name: 'Rumah Sakit',
        type: 'sub',
        icon: 'icofont icofont-hospital',
        children: [
          {
            state: 'rumahsakit-list',
            name: 'Data Rumah Sakit'
          },
          {
            state: 'rumahsakit-report',
            name: 'Laporan Rumah Sakit'
          }
        ]
      },
      {
        main_state : 'bjb',
        state: 'klinik',
        name: 'FKTP',
        type: 'sub',
        icon: 'icofont icofont-medical-sign-alt',
        children: [
          {
            state: 'klinik-list',
            name: 'Data FKTP'
          },
          {
            state: 'klinik-report',
            name: 'Laporan FKTP'
          }
        ]
      },
      {
        main_state : 'bjb',
        state: 'peserta',
        name: 'Anggota',
        type: 'sub',
        icon: 'icofont icofont-user-alt-3',
        children: [
          {
            state: 'peserta-list',
            name: 'Data Anggota'
          },
          {
            state: 'peserta-report',
            name: 'Laporan Anggota'
          }
        ]
      }
    ]
  },
];

@Injectable()
export class MenuItemsBJB {
  getAll(): Menu[] {
    return MENUITEMSBJB;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
