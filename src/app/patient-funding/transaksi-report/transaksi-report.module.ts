import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TransaksiReportComponent } from './transaksi-report.component';
import { TransaksiReportRoutes } from './transaksi-report.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(TransaksiReportRoutes),
      SharedModule
  ],
  declarations: [TransaksiReportComponent]
})

export class TransaksiReportModule {}
