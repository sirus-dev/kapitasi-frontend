import { Routes } from '@angular/router';

import { RumahSakitAddComponent } from './rumahsakit-add.component';

export const RumahSakitAddRoutes: Routes = [{
  path: '',
  component: RumahSakitAddComponent,
  data: {
    breadcrumb: 'Tambah Rumah Sakit',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
