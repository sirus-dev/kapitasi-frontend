import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { KlinikReportComponent } from './klinik-report.component';
import { KlinikReportRoutes } from './klinik-report.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KlinikReportRoutes),
      SharedModule
  ],
  declarations: [KlinikReportComponent]
})

export class KlinikReportModule {}
