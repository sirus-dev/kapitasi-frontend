import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RumahSakitAddComponent } from './rumahsakit-add.component';
import { RumahSakitAddRoutes } from './rumahsakit-add.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RumahSakitAddRoutes),
      SharedModule
  ],
  declarations: [RumahSakitAddComponent]
})

export class RumahSakitAddModule {}
