import { Routes } from '@angular/router';

import { TransaksiAddComponent } from './transaksi-add.component';

export const TransaksiAddRoutes: Routes = [{
  path: '',
  component: TransaksiAddComponent,
  data: {
    breadcrumb: 'Tambah Iuran',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
