import { Routes } from '@angular/router';

import { PerusahaanAddComponent } from './perusahaan-add.component';

export const PerusahaanAddRoutes: Routes = [{
  path: '',
  component: PerusahaanAddComponent,
  data: {
    breadcrumb: 'Tambah Perusahaan',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
