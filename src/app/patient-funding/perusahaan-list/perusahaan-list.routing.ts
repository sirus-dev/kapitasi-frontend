import { Routes } from '@angular/router';

import { PerusahaanListComponent } from './perusahaan-list.component';

export const PerusahaanListRoutes: Routes = [{
  path: '',
  component: PerusahaanListComponent,
  data: {
    breadcrumb: 'Daftar Perusahaan',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
