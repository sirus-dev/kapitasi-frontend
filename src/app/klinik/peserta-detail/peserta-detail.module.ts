import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PesertaDetailComponent } from './peserta-detail.component';
import { PesertaDetailRoutes } from './peserta-detail.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PesertaDetailRoutes),
      SharedModule
  ],
  declarations: [PesertaDetailComponent]
})

export class PesertaEditModule {}
